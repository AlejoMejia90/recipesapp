
# Descripción

En este repositorio encontrarás un proyecto desarrollado en Android Studio en lenguaje Kotlin como desarrollo de prueba tecnica, se implemento una arquitectura definida, patrones de diseño, clean arquitecture, persistencia de datos, test unitarios, inyeccion de dependencias y consumo de servicios REST.

# Caracteristicas

#### 1. Arquitectura

- MVVM (Model - View -ViewModel)

#### 2. Consumo de Servicios REST

- Retrofit 2

#### 3. Inyección de dependencias

- Dagger Hilt

#### 4. Persistencia de datos

- Room

#### 5. Unit Testing

- JUnit v.4
- Mock

#### 5. Patrones de diseño

- Patrones Creacionales

#### 6. Clean Arquitecture

![](https://cursokotlin.com/wp-content/uploads/2021/05/Android-Clean-Architecture.png)

# Problemas
La mayor dificultad presentada fue con la inyeccion de dependecias, ya que en la configuracion inicial del proyecto habian muchas librerias desactualizadas, por otro lado al usar Fragment-ViewModel la inyeccion del viewModel sobre el fragment debia hacerse de forma diferente, despues de mucho investigar encontre que la inyeccion del viewModel debia hacerse de la siguiente manera ***private val homeViewModel by viewModels<HomeViewModel>()***.

# Librerias

- Glide   https://github.com/bumptech/glide
- ViewModel https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-viewmodel-compose/2.5.1
- Retrofit https://mvnrepository.com/artifact/com.squareup.retrofit2/retrofit/2.9.0
- Gson https://mvnrepository.com/artifact/com.google.code.gson/gson/2.8.9
- Coroutines  https://github.com/Kotlin/kotlinx.coroutines
- Dagger Hilt  https://mvnrepository.com/artifact/com.google.dagger/hilt-android/2.35
- Room https://mvnrepository.com/artifact/androidx.room/room-ktx/2.4.0
- Mock https://github.com/mockk/mockk
