package com.alejandromejia.recipesapp.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.FoodRecipeSimple
import com.alejandromejia.recipesapp.data.model.Result
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.domain.GetRecipeByFilterUseCase
import com.alejandromejia.recipesapp.domain.GetRecipeById
import com.alejandromejia.recipesapp.domain.PutRecipeDbUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class SearchRecipesViewModelTest {

    @RelaxedMockK
    private lateinit var getRecipeByFilterUseCase: GetRecipeByFilterUseCase

    @RelaxedMockK
    private lateinit var getRecipeById: GetRecipeById

    @RelaxedMockK
    private lateinit var putRecipeDbUseCase: PutRecipeDbUseCase

    private lateinit var searchRecipesViewModel: SearchRecipesViewModel

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        searchRecipesViewModel =
            SearchRecipesViewModel(getRecipeById, getRecipeByFilterUseCase, putRecipeDbUseCase)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }

    @Test
    fun whenSearchAnyKeyWordApiReturnsARecipesListResult() = runTest {

        //Given
        val query = "query"
        val myResult = listOf(Result(1, "xxx", "xxx", "query"))
        val myRoot = FoodRecipeSimple(myResult)

        coEvery { getRecipeByFilterUseCase(query) } returns myRoot

        //When

        searchRecipesViewModel.getRecipesByFilter(query)

        //Then

        assert(searchRecipesViewModel.recipeSimple.value == myRoot)

    }

    @Test
    fun whenSearchRecipeByIdInApi() = runTest {

        //Given
        val id = 1
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myRoot = Root(1, 2, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true)

        coEvery { getRecipeById(id) } returns myRoot

        //When

        searchRecipesViewModel.searchRecipeById(id)

        //Then

        assert(searchRecipesViewModel.completeRecipe.value == myRoot)

    }


}