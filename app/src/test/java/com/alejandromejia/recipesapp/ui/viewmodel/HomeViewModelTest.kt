package com.alejandromejia.recipesapp.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.FoodRecipe
import com.alejandromejia.recipesapp.data.model.Recipe
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.domain.GetRecipeById
import com.alejandromejia.recipesapp.domain.GetRecipeUseCase
import com.alejandromejia.recipesapp.domain.PutRecipeDbUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class HomeViewModelTest {

    @RelaxedMockK
    private lateinit var getRecipeUseCase: GetRecipeUseCase

    @RelaxedMockK
    private lateinit var getRecipeById: GetRecipeById

    @RelaxedMockK
    private lateinit var putRecipeDbUseCase: PutRecipeDbUseCase

    private lateinit var homeViewModel: HomeViewModel


    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()


    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        homeViewModel = HomeViewModel(getRecipeUseCase, getRecipeById, putRecipeDbUseCase)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }

    @Test
    fun whenViewModelIsCreatedGetAllRandomRecipes() = runTest {

        //Given
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myResult = listOf(Recipe(1, 1, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true))
        val myFoodRecipe = FoodRecipe(myResult)

        coEvery { getRecipeUseCase() } returns myFoodRecipe

        //When

        homeViewModel.onCreate()

        //Then

        assert(homeViewModel.recipe.value == myFoodRecipe)

    }

    @Test
    fun whenSearchRecipeByIdInApi() = runTest {

        //Given
        val id = 1
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myRoot = Root(1, 2, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true)

        coEvery { getRecipeById(id) } returns myRoot

        //When

        homeViewModel.searchRecipeById(id)

        //Then

        assert(homeViewModel.completeRecipe.value == myRoot)

    }

}