package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.Root
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetAllRecipesDBUseCaseTest {

    @RelaxedMockK
    private lateinit var repository: RecipeRepository

    lateinit var getAllRecipesDBUseCase: GetAllRecipesDBUseCase

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        getAllRecipesDBUseCase = GetAllRecipesDBUseCase(repository)
    }

    @Test
    fun whenDataBaseReturnsACompleteRecipesList() = runBlocking {

        //Given
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myRoot = Root(1, 2, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true)
        val myListRoot = listOf(myRoot)

        coEvery { repository.getAllRecipesFromDB() } returns myListRoot

        //When

        getAllRecipesDBUseCase()

        //Then

        coVerify(exactly = 1) { repository.getAllRecipesFromDB() }

    }
}