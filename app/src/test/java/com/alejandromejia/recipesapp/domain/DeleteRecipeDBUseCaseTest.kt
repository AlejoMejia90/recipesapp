package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.ExtendedIngredient
import com.alejandromejia.recipesapp.data.model.Root
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class DeleteRecipeDBUseCaseTest {

    @RelaxedMockK
    private lateinit var repository: RecipeRepository

    lateinit var deleteRecipeDBUseCase: DeleteRecipeDBUseCase

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        deleteRecipeDBUseCase = DeleteRecipeDBUseCase(repository)
    }

    @Test
    fun whenDeleteARecipeFromDataBase() = runBlocking {

        //Given
        val myExtendedIngredient = listOf(ExtendedIngredient(1, "xxx", "xxx", "xxx"))
        val myRoot = Root(1, 2, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true)
        val myListRoot = listOf(Root(1, 2, myExtendedIngredient, "xxx", 1, 1, "xxx", "xxx", true))

        coEvery { repository.deleteRecipe(myRoot) } returns myListRoot

        //When

        deleteRecipeDBUseCase(myRoot)

        //Then

        coVerify(exactly = 1) { repository.deleteRecipe(myRoot) }

    }
}