package com.alejandromejia.recipesapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.domain.DeleteRecipeDBUseCase
import com.alejandromejia.recipesapp.domain.GetAllRecipesDBUseCase
import com.alejandromejia.recipesapp.domain.GetRecipeDBById
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoritesViewModel @Inject constructor(
    private val getAllRecipesDBUseCase: GetAllRecipesDBUseCase,
    private val getRecipeDBById: GetRecipeDBById,
    private val deleteRecipeDBUseCase: DeleteRecipeDBUseCase
) : ViewModel() {

    val completeRecipe = MutableLiveData<List<Root>>()
    val isLoading = MutableLiveData<Boolean>()
    val root = MutableLiveData<Root>()

    fun onCreate() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getAllRecipesDBUseCase()

            if (result != null) {
                completeRecipe.postValue(result!!)
                isLoading.postValue(false)
            }

        }

    }

    fun searchRecipeById(id: Int) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getRecipeDBById(id)

            if (result != null) {
                root.postValue(result!!)
                isLoading.postValue(false)
            }

        }
    }

    fun deleteFavorite(root: Root) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = deleteRecipeDBUseCase(root)
            completeRecipe.postValue(result)
            isLoading.postValue(false)

        }
    }


}