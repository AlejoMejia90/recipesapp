package com.alejandromejia.recipesapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alejandromejia.recipesapp.data.model.FoodRecipe
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.domain.GetRecipeById
import com.alejandromejia.recipesapp.domain.GetRecipeUseCase
import com.alejandromejia.recipesapp.domain.PutRecipeDbUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getRecipeUseCase: GetRecipeUseCase,
    private val getRecipeById: GetRecipeById,
    private val putRecipeDbUseCase: PutRecipeDbUseCase
) : ViewModel() {

    val recipe = MutableLiveData<FoodRecipe>()
    val completeRecipe = MutableLiveData<Root>()
    val isLoading = MutableLiveData<Boolean>()

    fun onCreate() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getRecipeUseCase()

            if (result != null) {
                recipe.postValue(result!!)
                isLoading.postValue(false)
            }
        }
    }

    fun searchRecipeById(id: Int) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getRecipeById(id)

            if (result != null) {
                completeRecipe.postValue(result!!)
                isLoading.postValue(false)
            }
        }
    }

    fun saveFavorite(fullRecipe: Root) {
        viewModelScope.launch {
            isLoading.postValue(true)
            putRecipeDbUseCase(fullRecipe)
            isLoading.postValue(false)
        }
    }
}