package com.alejandromejia.recipesapp.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alejandromejia.recipesapp.R
import com.alejandromejia.recipesapp.data.model.Result
import com.alejandromejia.recipesapp.ui.view.viewholder.RecipeSimpleViewHolder

class RecipeSimpleAdapter(
    private val recipeSimple: List<Result>,
    private val onClickListener: (Result) -> Unit
) : RecyclerView.Adapter<RecipeSimpleViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeSimpleViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return RecipeSimpleViewHolder(
            layoutInflater.inflate(
                R.layout.item_simple_recipe,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecipeSimpleViewHolder, position: Int) {
        val item = recipeSimple[position]
        holder.bind(item, onClickListener)
    }

    override fun getItemCount(): Int = recipeSimple.size
}