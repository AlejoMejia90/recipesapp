package com.alejandromejia.recipesapp.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alejandromejia.recipesapp.R
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.ui.view.viewholder.RecipeDBViewHolder

class RecipeDBAdapter(private val recipe: List<Root>, private val onClickListener: (Root) -> Unit) :
    RecyclerView.Adapter<RecipeDBViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeDBViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return RecipeDBViewHolder(layoutInflater.inflate(R.layout.item_recipe, parent, false))
    }

    override fun onBindViewHolder(holder: RecipeDBViewHolder, position: Int) {
        val item = recipe[position]
        holder.bind(item, onClickListener)
    }

    override fun getItemCount(): Int = recipe.size
}