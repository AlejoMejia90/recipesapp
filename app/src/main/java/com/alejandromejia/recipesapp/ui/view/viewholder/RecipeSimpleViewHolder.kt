package com.alejandromejia.recipesapp.ui.view.viewholder

import android.annotation.SuppressLint
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.alejandromejia.recipesapp.data.model.Result
import com.alejandromejia.recipesapp.databinding.ItemSimpleRecipeBinding
import com.bumptech.glide.Glide

class RecipeSimpleViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ItemSimpleRecipeBinding.bind(view)

    @SuppressLint("SetTextI18n")
    fun bind(recipe: Result, onClickListener: (Result) -> Unit) {

        binding.tvTitle.text = recipe.title
        Glide.with(binding.ivRecipeSimple.context).load(recipe.image).into(binding.ivRecipeSimple)
        binding.tvInformation.setOnClickListener { onClickListener(recipe) }
    }
}