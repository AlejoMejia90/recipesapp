package com.alejandromejia.recipesapp.ui.view.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.text.HtmlCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.alejandromejia.recipesapp.R
import com.alejandromejia.recipesapp.data.model.Result
import com.alejandromejia.recipesapp.data.model.Root
import com.alejandromejia.recipesapp.databinding.FragmentSearchRecipesBinding
import com.alejandromejia.recipesapp.ui.view.adapter.RecipeSimpleAdapter
import com.alejandromejia.recipesapp.ui.viewmodel.SearchRecipesViewModel
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchRecipesFragment : Fragment(), SearchView.OnQueryTextListener,
    SearchView.OnCloseListener {

    private var _binding: FragmentSearchRecipesBinding? = null
    private val binding get() = _binding!!
    private lateinit var mRecipeSimpleAdapter: RecipeSimpleAdapter
    lateinit var fullRecipe: Root
    private var recipesSimpleList = mutableListOf<Result>()
    private var flag: Boolean = false
    private val searchRecipesViewModel by viewModels<SearchRecipesViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentSearchRecipesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        initRecyclerViews()

        binding.rvSimpleRecipes.isVisible = true
        binding.svRecipe.isVisible = true
        binding.svSpecialFormMeasureInfo.isVisible = false
        flag = false

        binding.svRecipe.onActionViewExpanded()
        binding.svRecipe.setOnQueryTextListener(this)
        binding.svRecipe.setOnCloseListener(this)

        searchRecipesViewModel.recipeSimple.observe(this, Observer {

            val recipesSimple = it.results
            recipesSimpleList.clear()
            recipesSimpleList.addAll(recipesSimple)
            mRecipeSimpleAdapter.notifyDataSetChanged()

        })

        searchRecipesViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
        })

        searchRecipesViewModel.completeRecipe.observe(this, Observer {

            if (flag) {
                binding.svSpecialFormMeasureInfo.isVisible = true
                completeRecipe(it)
            } else {
                binding.svSpecialFormMeasureInfo.isVisible = false
            }

        })

        binding.btnBack.setOnClickListener {
            onClickBack()
        }

        binding.btnFavorites.setOnClickListener {
            onClickFavorites()
        }

        return root
    }

    private fun onClickFavorites() {

        binding.btnFavorites.setImageResource(R.drawable.ic_favorites)
        searchRecipesViewModel.saveFavorite(fullRecipe)
        Toast.makeText(binding.rvSimpleRecipes.context, "The recipe has been marked as a favorite", Toast.LENGTH_SHORT).show()
    }

    private fun onClickBack() {
        flag = false
        binding.rvSimpleRecipes.isVisible = true
        binding.svRecipe.isVisible = true
        binding.svSpecialFormMeasureInfo.isVisible = false
    }

    private fun completeRecipe(completeRecipe: Root?) {

        binding.btnFavorites.setImageResource(R.drawable.ic_uncheck_favorites)
        fullRecipe = completeRecipe!!

        val vegetarian: String
        val result = completeRecipe?.summary
        val ingredients = mutableListOf<String>()

        if (completeRecipe?.vegetarian == true) {
            vegetarian = "Vegetarian"
        } else {
            vegetarian = "Non Vegetarian"
        }

        Glide.with(binding.ivRecipe.context).load(completeRecipe?.image).into(binding.ivRecipe)
        binding.tvTitle.text = completeRecipe?.title
        binding.tvLikes.text = "${completeRecipe?.aggregateLikes} likes"
        binding.tvServings.text = "${completeRecipe?.servings} Servings"
        binding.tvTime.text = "${completeRecipe?.readyInMinutes} minutes"
        binding.tvVegetarian.text = "$vegetarian"
        binding.tvSummary.text =
            result?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }

        for (i in completeRecipe?.extendedIngredients!!) {

            val name = i.name

            var firstLtr = name.substring(0, 1)
            val restLtrs = name.substring(1, name.length)

            firstLtr = firstLtr.uppercase()
            var ingredientName = firstLtr + restLtrs

            ingredients.add("*  $ingredientName -- ${i.original}")

        }

        val separator = "\n\n"

        val ingredientsStr = ingredients.joinToString(separator)

        binding.tvIngredients.text = ingredientsStr

    }

    private fun initRecyclerViews() {

        mRecipeSimpleAdapter = RecipeSimpleAdapter(recipesSimpleList) {
            onItemSelected(it)
        }
        binding.rvSimpleRecipes.layoutManager = LinearLayoutManager(binding.rvSimpleRecipes.context)
        binding.rvSimpleRecipes.adapter = mRecipeSimpleAdapter


    }

    private fun onItemSelected(result: Result) {
        flag = true

        binding.rvSimpleRecipes.isVisible = false
        binding.svRecipe.isVisible = false

        searchRecipesViewModel.searchRecipeById(result.id)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onQueryTextSubmit(query: String?): Boolean {

        if (!query.isNullOrEmpty()) {
            searchRecipesViewModel.getRecipesByFilter(query)
        }

        val context = binding.rvSimpleRecipes.context

        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)

        return true

    }

    override fun onQueryTextChange(newText: String?): Boolean {

        if (newText.isNullOrEmpty()) {
            val recipesSimple = emptyList<Result>()
            recipesSimpleList.clear()
            recipesSimpleList.addAll(recipesSimple)
            mRecipeSimpleAdapter.notifyDataSetChanged()
        }

        return true
    }

    override fun onClose(): Boolean {

        return true
    }

}