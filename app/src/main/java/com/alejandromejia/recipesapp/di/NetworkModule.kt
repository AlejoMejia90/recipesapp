package com.alejandromejia.recipesapp.di

import android.content.Context
import androidx.room.Room
import com.alejandromejia.recipesapp.data.database.RecipeDataBase
import com.alejandromejia.recipesapp.data.database.dao.RecipeDao
import com.alejandromejia.recipesapp.data.network.RecipeApiClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.spoonacular.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideRecipeApiClient(retrofit: Retrofit): RecipeApiClient {
        return retrofit.create(RecipeApiClient::class.java)
    }

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        RecipeDataBase::class.java,
        "recipe_database"
    ).build()

    @Singleton
    @Provides
    fun getDao(dataBase: RecipeDataBase): RecipeDao {
        return dataBase.getRecipeDao()
    }


}