package com.alejandromejia.recipesapp.data.model


import com.google.gson.annotations.SerializedName

data class FoodRecipe(
    @SerializedName("recipes")
    val recipes: List<Recipe>
)