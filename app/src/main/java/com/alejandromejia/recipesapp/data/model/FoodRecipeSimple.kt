package com.alejandromejia.recipesapp.data.model


import com.google.gson.annotations.SerializedName

data class FoodRecipeSimple(

    @SerializedName("results")
    val results: List<Result>

)