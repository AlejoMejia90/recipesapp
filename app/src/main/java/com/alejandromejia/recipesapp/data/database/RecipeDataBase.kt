package com.alejandromejia.recipesapp.data.database

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.alejandromejia.recipesapp.data.database.dao.RecipeDao
import com.alejandromejia.recipesapp.data.database.entities.RootEntity

@Database(entities = [RootEntity::class], version = 1, exportSchema = false)
@TypeConverters(RecipesTypeConverter::class)
abstract class RecipeDataBase : RoomDatabase() {

    companion object {
        fun get(application: Application): RecipeDataBase {
            return Room.databaseBuilder(application, RecipeDataBase::class.java, "recipe_database")
                .build()
        }
    }

    abstract fun getRecipeDao(): RecipeDao


}