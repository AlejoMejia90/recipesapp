package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.Root
import javax.inject.Inject


class GetRecipeById @Inject constructor(private val repository: RecipeRepository) {

    suspend operator fun invoke(id: Int): Root = repository.getCompleteRecipe(id)

}