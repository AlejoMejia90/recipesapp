package com.alejandromejia.recipesapp.domain

import com.alejandromejia.recipesapp.data.RecipeRepository
import com.alejandromejia.recipesapp.data.model.Root
import javax.inject.Inject

class DeleteRecipeDBUseCase @Inject constructor(private val repository: RecipeRepository) {

    suspend operator fun invoke(root: Root): List<Root> = repository.deleteRecipe(root)

}